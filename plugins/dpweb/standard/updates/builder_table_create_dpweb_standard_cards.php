<?php namespace DpWeb\Standard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDpWebStandardCards extends Migration
{
    public function up()
    {
        Schema::create('dpweb_standard_cards', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('title', 255);
            $table->text('content')->nullable();
            $table->string('link', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dpweb_standard_cards');
    }
}
