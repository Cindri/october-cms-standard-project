<?php namespace DpWeb\Standard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDpwebStandardCollectionitem extends Migration
{
    public function up()
    {
        Schema::create('dpweb_standard_collectionitem', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('collection_id');
            $table->string('title', 255);
            $table->string('link_url', 255)->nullable();
            $table->string('secondary_icon', 63)->nullable();
            $table->string('secondary_action', 255)->nullable();
            $table->string('avatar_image')->nullable();
            $table->text('avatar_content')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dpweb_standard_collectionitem');
    }
}
