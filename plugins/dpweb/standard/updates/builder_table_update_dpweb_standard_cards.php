<?php namespace DpWeb\Standard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDpwebStandardCards extends Migration
{
    public function up()
    {
        Schema::table('dpweb_standard_cards', function($table)
        {
            $table->string('page', 255);
        });
    }
    
    public function down()
    {
        Schema::table('dpweb_standard_cards', function($table)
        {
            $table->dropColumn('page');
        });
    }
}
