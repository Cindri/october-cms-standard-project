<?php namespace DpWeb\Standard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDpwebStandardCollection extends Migration
{
    public function up()
    {
        Schema::table('dpweb_standard_collection', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('dpweb_standard_collection', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
