<?php namespace DpWeb\Standard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDpwebStandardCarouselitem extends Migration
{
    public function up()
    {
        Schema::create('dpweb_standard_carouselitem', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('href', 127)->nullable();
            $table->string('image', 255)->nullable();
            $table->text('content')->nullable();
            $table->string('bgcolor', 31)->nullable();
            $table->integer('carousel_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dpweb_standard_carouselitem');
    }
}
