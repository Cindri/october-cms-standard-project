<?php namespace DpWeb\Standard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateDpwebStandardCollection extends Migration
{
    public function up()
    {
        Schema::create('dpweb_standard_collection', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('header', 255)->nullable();
            $table->string('type', 127)->default('basic');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('dpweb_standard_collection');
    }
}
