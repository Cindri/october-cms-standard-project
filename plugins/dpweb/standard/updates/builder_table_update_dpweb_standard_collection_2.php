<?php namespace DpWeb\Standard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDpwebStandardCollection2 extends Migration
{
    public function up()
    {
        Schema::table('dpweb_standard_collection', function($table)
        {
            $table->string('pages', 255);
        });
    }
    
    public function down()
    {
        Schema::table('dpweb_standard_collection', function($table)
        {
            $table->dropColumn('pages');
        });
    }
}
