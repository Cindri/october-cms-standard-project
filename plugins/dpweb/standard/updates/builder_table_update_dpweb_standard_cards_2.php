<?php namespace DpWeb\Standard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDpwebStandardCards2 extends Migration
{
    public function up()
    {
        Schema::table('dpweb_standard_cards', function($table)
        {
            $table->renameColumn('page', 'pages');
        });
    }
    
    public function down()
    {
        Schema::table('dpweb_standard_cards', function($table)
        {
            $table->renameColumn('pages', 'page');
        });
    }
}
