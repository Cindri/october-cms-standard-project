<?php namespace DpWeb\Standard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateDpwebStandardCollectionitem extends Migration
{
    public function up()
    {
        Schema::table('dpweb_standard_collectionitem', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('avatar_image')->change();
        });
    }
    
    public function down()
    {
        Schema::table('dpweb_standard_collectionitem', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->string('avatar_image', 191)->change();
        });
    }
}
