<?php

namespace DpWeb\Standard;

/**
 * The plugin.php file (called the plugin initialization script) defines the plugin information class.
 */

use AnandPatel\WysiwygEditors\FormWidgets\Editor;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'DP Standard plugins',
            'description' => 'Provides standard plugins occuring in most pages.',
            'author'      => 'David Peter',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerComponents()
    {
        return [
            '\DpWeb\Standard\Components\MainMenu' => 'MainMenu',
            '\DpWeb\Standard\Components\CardList' => 'CardList',
            '\DpWeb\Standard\Components\Collections' => 'Collections',
            '\DpWeb\Standard\Components\Carousels' => 'Carousels'
        ];
    }
}
