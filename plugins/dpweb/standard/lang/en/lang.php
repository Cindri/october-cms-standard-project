<?php return [
    'plugin' => [
        'name' => 'Standard',
        'description' => '',
    ],
    'cards:title' => 'Card title',
    'cards' => [
        'title' => [
            'placeholder' => 'Enter a title here',
        ],
        'content' => 'Card content',
        'link' => 'Link',
        'page' => 'Page',
    ],
    'backend-menu-elements' => 'Elements',
    'menu-backend-cards' => 'Cards',
    'collections' => [
        'updatedAt' => 'Updated at',
        'header' => 'Headline / Title',
        'type' => 'Render type',
        'pages' => 'Pages',
        'collections' => 'Collections',
    ],
];