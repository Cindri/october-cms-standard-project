<?php

namespace DpWeb\Standard\Components;

use DpWeb\Standard\Models\Card;

class CardList extends AbstractComponent
{
    public function componentDetails()
    {
        return [
            'name'        => 'Card list',
            'description' => 'Displays a set of cards defined in the backend.'
        ];
    }

    public $cardList;

    public $currentPageCardList;


    public function onRun() {
        $cardList = Card::all();
        $this->cardList = $cardList;
        $this->currentPageCardList = $this->filterByPage($cardList);
    }

}