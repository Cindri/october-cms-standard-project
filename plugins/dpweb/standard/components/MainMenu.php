<?php

namespace DpWeb\Standard\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Symfony\Component\HttpFoundation\Request;

class MainMenu extends ComponentBase
{

    public $menuItems;

    public function componentDetails()
    {
        return [
            'name'        => 'Main Menu',
            'description' => 'Renders the main menu of the page.'
        ];
    }

    public function onRun()
    {
        $baseUrl = Request::createFromGlobals()->getBaseUrl();
        $currentTheme = Theme::getEditTheme();
        $allPages = Page::listInTheme($currentTheme, true);
        $menuPages = [];
        foreach ($allPages as $menuItem) {
            $menuPages[] = ['url' => $baseUrl . $menuItem->url, 'title' => $menuItem->title];
        }
        $this->menuItems = $menuPages;
    }

    public function menu() {
        return $this->menuItems;
    }

}