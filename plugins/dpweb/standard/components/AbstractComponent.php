<?php

namespace DpWeb\Standard\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Collection;

abstract class AbstractComponent extends ComponentBase
{
    /**
     * Filters the collection items by current page (if they have a 'pages' property)
     *
     * @param Collection $itemList
     * @return Collection
     */
    public function filterByPage($itemList) {
        $filter = function ($item) {
            if (empty($item->pages)) {
                return false;
            }
            if (!is_array($item->pages)) {
                $item->pages = json_decode($item->pages);
            }
            return in_array($this->page->baseFileName, $item->pages);
        };
        return $itemList->filter($filter);
    }
}