<?php

namespace DpWeb\Standard\Components;

use DpWeb\Standard\Models\Collection;

class Collections extends AbstractComponent
{
    public function componentDetails()
    {
        return [
            'name'        => 'Collections',
            'description' => 'Displays a Materialize Collection'
        ];
    }

    public $collectionList;

    public $currentPageCollectionList;

    public function onRun() {
        $this->setUp();
    }

    protected function setUp() {
        $collectionList = Collection::all();
        $this->collectionList = $collectionList;
        $this->currentPageCollectionList = $this->filterByPage($collectionList);
    }

}