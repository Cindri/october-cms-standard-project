<?php

namespace DpWeb\Standard\Components;

use DpWeb\Standard\Models\Carousel;

class Carousels extends AbstractComponent
{
    public function componentDetails()
    {
        return [
            'name'        => 'Carousels',
            'description' => 'Displays Materialize Carousels'
        ];
    }

    public $carouselList;

    public $currentPageCarouselList;

    public function onRun() {
        $this->setUp();
    }

    protected function setUp() {
        $carouselList = Carousel::all();
        $this->carouselList = $carouselList;
        $this->currentPageCarouselList = $this->filterByPage($carouselList);
    }

}