<?php namespace DpWeb\Standard\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;

class Collection extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
        'Backend\Behaviors\RelationController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('DpWeb.Standard', 'main-menu-elements', 'side-menu-item2');
        $this->addCss('/plugins/dpweb/standard/assets/css/backend/collection.css');
    }

}
