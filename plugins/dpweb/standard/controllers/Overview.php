<?php

namespace DpWeb\Standard\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;

class Overview extends Controller
{
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('DpWeb.Standard', 'main-menu-elements', 'side-menu-item1');
    }

    public function index() {
        $this->addCss('/plugins/dpweb/standard/assets/css/backend/overview.css');
        return $this->makePartial('index');
    }
}