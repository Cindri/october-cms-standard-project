<?php namespace DpWeb\Standard\Models;

use Cms\Classes\Page;
use Cms\Classes\Theme;
use Model;

/**
 * Model
 */
class Card extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title', 'content', 'link'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'dpweb_standard_cards';

    public $jsonable = ['pages'];

    /**
     * Giving information for the page select dropdown
     *
     * @param string $value     Name of the form field
     * @param $formData         Form Data
     * @return array
     */
    public function getPagesOptions($value, $formData) {
        $pageList = Page::sortBy('baseFileName')->lists('title', 'baseFileName');
        return $pageList;
    }
}
