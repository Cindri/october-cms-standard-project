<?php namespace DpWeb\Standard\Models;

use Cms\Classes\Page;
use Model;

/**
 * Model
 */
class Carousel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \DpWeb\Standard\Traits\TranslatableRelation;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'dpweb_standard_carousel';

    public $jsonable = ['options', 'pages'];

    public $hasMany = [
        'carouselItems' => ['DpWeb\Standard\Models\CarouselItem']
    ];

    /**
     * Giving information for the page select dropdown
     * @return array
     */
    public function getPagesOptions($value, $formData) {
        $pageList = Page::sortBy('baseFileName')->lists('title', 'baseFileName');
        return $pageList;
    }
}
