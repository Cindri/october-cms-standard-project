<?php namespace DpWeb\Standard\Models;

use Cms\Classes\Page;
use Model;

/**
 * Model
 */
class Collection extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \DpWeb\Standard\Traits\TranslatableRelation;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'dpweb_standard_collection';

    public $jsonable = ['pages'];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['header'];

    public $hasMany = [
        'collectionItems' => ['DpWeb\Standard\Models\CollectionItem']
    ];

    /**
     * Giving information for the page select dropdown
     * @return array
     */
    public function getPagesOptions() {
        $pageList = Page::sortBy('baseFileName')->lists('title', 'baseFileName');
        return $pageList;
    }
}
