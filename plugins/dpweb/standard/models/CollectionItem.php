<?php namespace DpWeb\Standard\Models;

use Model;

/**
 * Model
 */
class CollectionItem extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \DpWeb\Standard\Traits\TranslatableRelation;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'dpweb_standard_collectionitem';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title', 'link_url', 'secondary_icon', 'secondary_action', 'avatar_image', 'avatar_content'];

    public $belongsTo = [
        'collection' => ['DpWeb\Standard\Models\Collection']
    ];

    public function getCollectionIdOptions() {
        $collectionList = Collection::all(['id', 'header']);
        $return = [];
        foreach ($collectionList as $collection) {
            $return[$collection->id] = $collection->header;
        }
        return $return;
    }

}
