<?php namespace DpWeb\Standard\Models;

use Model;

/**
 * Model
 */
class CarouselItem extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    use \DpWeb\Standard\Traits\TranslatableRelation;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['href', 'image', 'content', 'bgcolor'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'dpweb_standard_carouselitem';

    public $belongsTo = [
        'collection' => ['DpWeb\Standard\Models\Collection']
    ];
}
